from math import hypot, pi
from tkinter import Canvas

from configs import *


class Planet:

    def __init__(
            self,
            weight=5.972e24,
            coords=(149e9, 0),
            speed=(0, 29e3),
            color="#705211",
            radius=3e8):

        self.weight = weight
        self.x, self.y = coords
        self.sx, self.sy = speed
        self.radius = radius
        self.color = color

    def update(self, planets: list, delta_t):
        ax = ay = 0
        for planet in planets:
            if planet is not self:
                xdist = self.x - planet.x
                ydist = self.y - planet.y
                dist = hypot(xdist, ydist)
                if planet.weight < self.weight and dist < (planet.radius + self.radius):
                    self.sx = (self.sx * self.weight + planet.sx * planet.weight) / (self.weight + planet.weight)
                    self.weight += planet.weight
                    self.radius = ((4 / 3 * pi * (self.radius ** 3) + 4 / 3 * pi * (
                            planet.radius ** 3)) * 3 / 4 / pi) ** (1 / 3)
                    planets.remove(planet)
                    continue
                accel = - (G * planet.weight / (dist ** 2))
                ax += accel * xdist / dist
                ay += accel * ydist / dist
        self.sx += ax * delta_t
        self.sy += ay * delta_t
        self.x += self.sx * delta_t
        self.y += self.sy * delta_t

    def display(self, canvas: Canvas, scale, center):
        x = self.x / scale + center[0]
        y = self.y / scale + center[1]
        radius = self.radius / scale
        canvas.create_oval((x - radius, y - radius, x + radius, y + radius), fill=self.color)
