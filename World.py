from random import randint
from Planet import Planet
from time import time, sleep
from configs import world_speed


class World:
    def __init__(self):
        self.running = False
        self.t = time()
        self.planets = []
        self.planets.append(Planet(2e30, (0, 0), (0, 0), "#ffff00", 20e8))
        self.planets.append(Planet())
        self.planets.append(Planet(coords=(-149e9, 0)))
        for i in range(100):
            self.planets.append(
                Planet(
                    randint(0, int(1e27)),
                    (randint(-int(1e11), int(1e11)),
                     randint(-int(1e11), int(1e11))),
                    (randint(-int(1e5), int(1e5)),
                     randint(-int(1e5), int(1e5)))))

    def stop(self):
        self.running = False

    def start(self):
        self.running = True
        while self.running:
            delta_t = (self.t - time()) * world_speed
            self.t = time()
            for planet in self.planets:
                planet.update(self.planets, delta_t)
            sleep(.01)
