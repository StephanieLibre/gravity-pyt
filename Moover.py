class Moover:
    def __init__(self, window):
        self.mooving = False
        self.last_point = (0, 0)
        self.listeners = []
        window.bind('<Button-1>', self.start_moove)
        window.bind('<Motion>', self.do_moove)
        window.bind('<ButtonRelease-1>', self.end_moove)

    def bind(self, listener):
        self.listeners.append(listener)

    def start_moove(self, event):
        self.last_point = (event.x, event.y)
        self.mooving = True

    def do_moove(self, event):
        if self.mooving:
            for listener in self.listeners:
                listener((event.x - self.last_point[0], event.y - self.last_point[1]))
        self.last_point = (event.x, event.y)

    def end_moove(self, *args):
        self.mooving = False
