G = 6.674e-11             # Newton constant
world_speed = 80000       # Simulation speed
screen_width = 1600       # Screen width in pixels
screen_height = 800       # Screen height in pixels
sleepTime = 0.001         # sleep time between each world update
space_color = "#000020"   # Space background color
